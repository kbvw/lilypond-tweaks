% This file contains functions adapted from those found in LilyPond, 
% the GNU music typesetter. LilyPond source code is available from
% https://lilypond.org/download.html.
%
% Copyright (C) 1996--2022 Han-Wen Nienhuys <hanwen@xs4all.nl>
%                          Jan Nieuwenhuizen <janneke@gnu.org>
%
%               2012--2022 David Nalesnik <david.nalesnik@gmail.com>
%                          Thomas Morley <thomasmorley65@gmail.com>
%                          Dan Eble <nine.fierce.ballads@gmail.com>
%                          Jonas Hahnfeld <hahnjo@hahnjo.de>
%                          Jean Abou Samra <jean@abou-samra.fr>
%
% Specific attributions are displayed throughout this file.
%
% LilyPond is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% LilyPond is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with LilyPond.  If not, see <http://www.gnu.org/licenses/>.


\version "2.23.82"

over = \withMusicProperty bass ##t \etc

diminished = \markup {
  \fontsize #3 #(ly:wide-char->utf-8 #x2218)
}

augmented = \markup {
  \fontsize #3 #(ly:wide-char->utf-8 #x002b)
}

% HarmonicBackground context adapted from ChordNames context,
% found in the LilyPond file engraver-init.ly.
%
% Copyright (C) 1996--2022 Han-Wen Nienhuys <hanwen@xs4all.nl>
%                          Jan Nieuwenhuizen <janneke@gnu.org>
%
% Changes: 
% - Swap in the new Harmonic_background_engraver defined
%   below for the original Current_chord_text_engraver.
% - Inherit acceptability from ChordNames context.

\layout {
  \context {
    \type Engraver_group
    \name HarmonicBackground
    \alias Staff			% Catch Staff-level overrides like
				% \key, \transposition
    \description "Typesets pitch names with harmonic background information."
  
    \consists Output_property_engraver
    \consists Separating_line_group_engraver
    \consists Harmonic_background_engraver
    \consists Chord_name_engraver
    \consists Axis_group_engraver
    %\consists Note_spacing_engraver
    \consists Alteration_glyph_engraver
    
    noChordSymbol = ""
  
    \override VerticalAxisGroup.remove-first = ##t
    \override VerticalAxisGroup.remove-empty = ##t
    \override VerticalAxisGroup.staff-affinity = #DOWN
    \override VerticalAxisGroup.nonstaff-relatedstaff-spacing.padding = #0.5
    \override VerticalAxisGroup.nonstaff-nonstaff-spacing.padding = #0.5
    \override Parentheses.font-size = #1.5
  }
  \inherit-acceptability HarmonicBackground ChordNames
}

% harmonic-background-name function roughly based on
% ignatzek-chord-names function found in the LilyPond file
% chord-ignatzek-names.scm.
%
% Copyright (C) 2000--2022 Han-Wen Nienhuys <hanwen@xs4all.nl>
%
% Changes: 
% - Remove logic that calculates chord symbol from pitches.
% - Calculate chord symbol from root, bass and suffix,
%   the latter passed directly as markup.

#(define (harmonic-background-name root bass suffix-markup context)
   
   (let* ((name-root (ly:context-property context 'chordRootNamer))
          (slash-sep (ly:context-property context 'slashChordSeparator))
          (root-markup (name-root root #f))
          (mod-markup (make-super-markup suffix-markup))
          (markup (if (ly:pitch? bass)
                      (list 
                        root-markup 
                        mod-markup
                        slash-sep 
                        (name-root bass #f))
                      (list 
                        root-markup 
                        mod-markup))))
     
     (make-line-markup markup)))

% Harmonic_background_engraver adapted from Current_chord_text_engraver
% found in the LilyPond file scheme-engravers.scm.
%
% Copyright (C) 2012--2022 David Nalesnik <david.nalesnik@gmail.com>
%                          Thomas Morley <thomasmorley65@gmail.com>
%                          Dan Eble <nine.fierce.ballads@gmail.com>
%                          Jonas Hahnfeld <hahnjo@hahnjo.de>
%                          Jean Abou Samra <jean@abou-samra.fr>
%
% Changes: 
% - Additionally listen for text-script-events.
% - Remove logic to store all pitches and inversion.
% - Instead, store the first encountered pitch as root, the first 
%   pitch marked as bass, and the first encountered text-script 
%   as suffix.
% - Pass these to naming function instead of all pitches and inversion.

#(define (Harmonic_background_engraver context)
  (let ((note-events '())
        (rest-event #f)
        (text-script-events '()))
    
    (make-engraver
     (listeners
      ((note-event engraver event)
       (set! note-events (cons event note-events)))
      ((general-rest-event engraver event #:once)
       (set! rest-event event))
      ((text-script-event engraver event)
       (set! text-script-events (cons event text-script-events))))
     
     ((pre-process-music engraver)
      (cond
       
       (rest-event
        (ly:context-set-property! context 'currentChordCause rest-event)
        (let ((symbol (ly:context-property context 'noChordSymbol)))
          (ly:context-set-property! context 'currentChordText symbol)))
       
       ((pair? note-events)
        (ly:context-set-property! context 'currentChordCause (car note-events))
        (let ((root '())
              (bass '())
              (suffix '()))
          (for-each
           (lambda (text-ev)
             (let ((text (ly:event-property text-ev 'text)))
               (when (markup? text)
                 (set! suffix text))))
           text-script-events)
          (for-each
           (lambda (note-ev)
             (let ((pitch (ly:event-property note-ev 'pitch)))
               (when (ly:pitch? pitch)
                 (let ((is-bass (ly:event-property note-ev 'bass #f)))
                   (when is-bass
                       (set! bass pitch))
                   (set! root pitch)))))
           note-events)
          (let ((text (harmonic-background-name root bass suffix context)))
            (ly:context-set-property! context 'currentChordText text))))
       
       (else
        (ly:context-set-property! context 'currentChordCause #f)
        (ly:context-set-property! context 'currentChordText #f)))) 
     
     ((stop-translation-timestep engraver)
      (set! note-events '())
      (set! rest-event #f)
      (set! text-script-events '())))))

#(ly:register-translator
 Harmonic_background_engraver 'Harmonic_background_engraver
 '((events-accepted . (note-event general-rest-event text-script-event))
   (properties-read . (chordRootNamer
                       noChordSymbol))
   (properties-written . (currentChordCause currentChordText))
   (description . "Catch note, rest and text-script events and generate
the appropriate harmonic symbol. Actually creating a chord name grob is
left to other engravers.")))